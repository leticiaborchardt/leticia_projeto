<?
include "includes/uteis.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="css/login.css">
</head>

<body class="bg-image">

    <main class="container">
        <div class="row rowContainer">
                <form action="<?=$url_site?>/controllers/restrito.php" class="form-signin text-white divForm" method="POST">
                        <div class="col-12 col-md-10 col-sm-12 text-center">
                            <h1>Bem-Vindo(a)!</h1>
                        </div>
                    <div class="form-group col-12 col-md-10 col-sm-12">
                        <label for="usuario">Login</label>
                        <input type="text" name="usuario" id="usuario" class="form-control" required>
                    </div>
                    <div class="form-group col-12 col-md-10 col-sm-12">
                        <label for="senha">Senha</label>
                        <input type="password" name="senha" id="senha" class="form-control" required>
                    </div>
                    <div class="col-12 col-sm-12">
                        <button type="submit" class="btn btn-warning">Entrar</button>
                    </div>
                </form>
        </div>
    </main>

    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/app.js?v=<?= rand(0, 9999) ?>"></script>
    <? if (isset($_GET['msg'])) { ?>
        <script type="text/javascript">
        $(function() {
            myAlert('danger','<?=$_GET['msg']?>', 'main');
        })
        </script>
    <?}?>
    
</body>

</html>