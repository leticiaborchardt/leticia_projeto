<?
    $navCadastro = array(
        'cadastroAdministradora' => '<i class="bi bi-clipboard2-check-fill"></i> Cadastrar Administradora',
        'cadastroCondominio' => '<i class="bi bi-building"></i> Cadastrar Condomínios',
        'cadastroBloco' => '<i class="bi bi-plus-square"></i> Cadastrar Blocos',
        'cadastroUnidade' => '<i class="bi bi-house-fill"></i> Cadastrar Unidades',
        'cadastroMorador' => '<i class="bi bi-person-plus-fill"></i> Cadastrar Moradores',
        'cadastroConselho' => '<i class="bi bi-briefcase-fill"></i> Cadastrar Conselho',
        'cadastroUsuario' => '<i class="bi bi-person-circle"></i> Cadastrar Usuário no Sistema'   
    );

    $navListagem = array(
        'listaAdministradora' => '<i class="bi bi-clipboard2-check-fill"></i> Listar Administradoras',
        'listaCondo' => '<i class="bi bi-view-list"></i> Listar Condomínios',
        'listaBloco' => '<i class="bi bi-list-task"></i> Listar Blocos',
        'listaUnidade' => '<i class="bi bi-list-ol"></i> Listar Unidades',
        'listaMorador' => '<i class="bi bi-person-lines-fill"></i> Listar Moradores',
        'listaConselho' => '<i class="bi bi-briefcase-fill"></i> Listar Conselhos',
        'listaUsuario' => '<i class="bi bi-person-circle"></i> Listar Usuários do Sistema' 
    );
?>