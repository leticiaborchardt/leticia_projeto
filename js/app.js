$(function () {
    $("#cadastroMorador").submit(function () {
        var editar = $(this).find('input[name="editar"]').val();
       
        var url;
        var urlRedir;
        if(editar){
            url = url_site+'api/editaMorador.php';
            urlRedir = url_site+'listaMorador';
        } else{
            url = url_site+'api/cadastraMorador.php';
            urlRedir = url_site+'cadastroMorador';
        }

        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),

            success : function(data){
                if(data.status == 'success'){
                   // $('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });

        return false;
    });

    //deletar moradores
    $('#listaMoradores').on('click','.removerMorador' ,function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: url_site+'api/deletaMorador.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro},
            success : function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', url_site+'listaMorador');
                }else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })


    // chama bloco após selecionar o condomínio
    $('.fromCondominio').change(function(){
        selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/listBlocos.php',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado },
            success : function(data) {
                selectPopulation('.fromBloco', data.resultSet, 'nomeBloco');
            }
        })
        
    })

    //chama unidade após selecionar o bloco
    $('.fromBloco').change(function() {
        selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/listUnidades.php',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado },
            success : function(data) {
                selectPopulation('.fromUnidade', data.resultSet, 'nomeUnidade');
            }
        })
        
    })

    function selectPopulation(seletor, dados, field) {
        estrutura = '<option value:"">Selecione...</option>';

        for (let i = 0; i < dados.length; i++) {
            
            estrutura += '<option value="'+dados[i].id+'">'+dados[i][field]+'</option>';
        }

        $(seletor).html(estrutura);
    }




//-----------------CADASTRO ADMINISTRADORA-----------------------

$('#cadastroAdm').submit(function(){
    var editar = $(this).find('input[name="editar"]').val();
    var url;
    var urlRedir;
    if(editar){
        url = url_site+'api/editaAdministradora.php';
        urlRedir = url_site+'listaAdministradora';
    } else{
        url = url_site+'api/cadastraAdministradora.php';
        urlRedir = url_site+'cadastroAdministradora';
    }

    $('.buttonEnviar').attr('disabled', 'disabled');

    $.ajax({
        url: url,
        dataType: 'json',
        type: 'POST',
        data: $(this).serialize(),
        success : function(data){
            console.log(data);
            if(data.status == 'success'){
                //$('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                myAlert(data.status,data.msg,'main', urlRedir);
            } else {
                myAlert(data.status,data.msg,'main', urlRedir);
            }
        }
    });

    return false;
});

$('#listaAdm').on('click','.removerAdministradora' ,function(){
    var idRegistro = $(this).attr('data-id');
    $.ajax({
        url: url_site+'api/deletaAdministradora.php',
        dataType: 'json',
        type: 'POST',
        data: { id: idRegistro},
        success : function(data){
            if(data.status == 'success'){
                myAlert(data.status,data.msg,'main', url_site+'listaAdministradora');
            } else {
                myAlert(data.status,data.msg,'main');
            }
        }
    })
    return false;
});



//-----------------CADASTRO CONDOMINIO-----------------------

$('#cadastroCondominio').submit(function(){
    var editar = $(this).find('input[name="editar"]').val();
    var url;
    var urlRedir;
    if(editar){
        url = url_site+'api/editaCondominio.php';
        urlRedir = url_site+'listaCondo';
    } else{
        url = url_site+'api/cadastraCondominio.php';
        urlRedir = url_site+'cadastroCondominio';
    }

    $('.buttonEnviar').attr('disabled', 'disabled');

    $.ajax({
        url: url,
        dataType: 'json',
        type: 'POST',
        data: $(this).serialize(),
        success : function(data){
            console.log(data);
            if(data.status == 'success'){
                //$('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                myAlert(data.status,data.msg,'main', urlRedir);
            } else {
                myAlert(data.status,data.msg,'main', urlRedir);
            }
        }
    });

    return false;
});

$('#listaCondominio').on('click','.removerCondominio' ,function(){
    var idRegistro = $(this).attr('data-id');
    $.ajax({
        url: url_site+'api/deletaCondominio.php',
        dataType: 'json',
        type: 'POST',
        data: { id: idRegistro},
        success : function(data){
            if(data.status == 'success'){
                myAlert(data.status,data.msg,'main', url_site+'listaCondo');
            } else {
                myAlert(data.status,data.msg,'main');
            }
        }
    })
    return false;
});

// ---------------CADASTRO BLOCO---------------

$('#cadastroBloco').submit(function(){
    var editar = $(this).find('input[name="editar"]').val();
    var url;
    var urlRedir;
    if(editar){
        url = url_site+'api/editaBloco.php';
        urlRedir = url_site+'listaBloco';
    } else{
        url = url_site+'api/cadastraBloco.php';
        urlRedir = url_site+'cadastroBloco';
    }

    $('.buttonEnviar').attr('disabled', 'disabled');

    $.ajax({
        url: url,
        dataType: 'json',
        type: 'POST',
        data: $(this).serialize(),
        success : function(data){
            if(data.status == 'success'){
                //$('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                myAlert(data.status,data.msg,'main', urlRedir);
            } else {
                myAlert(data.status,data.msg,'main', urlRedir);
            }
        }
    });

    return false;
});

$('#listaBloco').on('click','.removerBloco' ,function(){
    var idRegistro = $(this).attr('data-id');
    $.ajax({
        url: url_site+'api/deletaBloco.php',
        dataType: 'json',
        type: 'POST',
        data: { id: idRegistro},
        success : function(data){
            if(data.status == 'success'){
                myAlert(data.status,data.msg,'main',url_site+'listaBloco');
            } else {
                myAlert(data.status,data.msg,'main');
            }
        }
    })
    return false;
});


//-----------------CADASTRO UNIDADES-----------------------

$('#cadastroUnidade').submit(function(){
    var editar = $(this).find('input[name="editar"]').val();
    var url;
    var urlRedir;
    if(editar){
        url = url_site+'api/editaUnidade.php';
        urlRedir = url_site+'listaUnidade';
    } else{
        url = url_site+'api/cadastraUnidade.php';
        urlRedir = url_site+'cadastroUnidade';
    }

    $('.buttonEnviar').attr('disabled', 'disabled');

    $.ajax({
        url: url,
        dataType: 'json',
        type: 'POST',
        data: $(this).serialize(),
        success : function(data){
            if(data.status == 'success'){
                //$('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                myAlert(data.status,data.msg,'main', urlRedir);
            } else {
                myAlert(data.status,data.msg,'main', urlRedir);
            }
        }
    });

    return false;
});

$('#listaUnidade').on('click','.removerUnidade' ,function(){
    var idRegistro = $(this).attr('data-id');
    $.ajax({
        url: url_site+'api/deletaUnidade.php',
        dataType: 'json',
        type: 'POST',
        data: { id: idRegistro},
        success : function(data){
            if(data.status == 'success'){
                myAlert(data.status,data.msg,'main', url_site+'listaUnidade');
            } else {
                myAlert(data.status,data.msg,'main');
            }
        }
    })
    return false;
});


//-----------------CADASTRO CONSELHO-----------------------

$('#cadastroConselho').submit(function(){
    var editar = $(this).find('input[name="editar"]').val();
    var url;
    var urlRedir;
    if(editar){
        url = url_site+'api/editaConselho.php';
        urlRedir = url_site+'listaConselho';
    } else{
        url = url_site+'api/cadastraConselho.php';
        urlRedir = url_site+'cadastroConselho';
    }

    $('.buttonEnviar').attr('disabled', 'disabled');

    $.ajax({
        url: url,
        dataType: 'json',
        type: 'POST',
        data: $(this).serialize(),
        success : function(data){
            if(data.status == 'success'){
                //$('.totalRegistros').html('Total Registros: '+data.totalRegistros);
                myAlert(data.status,data.msg,'main', urlRedir);
            } else {
                myAlert(data.status,data.msg,'main', urlRedir);
            }
        }
    });

    return false;
});

$('#listaConselho').on('click','.removerConselho' ,function(){
    var idRegistro = $(this).attr('data-id');
    $.ajax({
        url: url_site+'api/deletaConselho.php',
        dataType: 'json',
        type: 'POST',
        data: { id: idRegistro},
        success : function(data){
            if(data.status == 'success'){
                myAlert(data.status,data.msg,'main', url_site+'listaConselho');
            } else {
                myAlert(data.status,data.msg,'main');
            }
        }
    })
    return false;
});

//-----------------CADASTRO USUARIO-----------------------

$("#cadastroUsuario").submit(function(){
    var senha = $(this).find('#senha').val();
    var confirmaSenha = $(this).find('#csenha').val();

    var editar = $(this).find('input[name="editar"]').val();
    var url;
    var urlRedir;
    if(editar){
        url = url_site+'api/editaUsuario.php';
        urlRedir = url_site+'listaUsuario';
    } else{
        url = url_site+'api/cadastraUsuario.php';
        urlRedir = url_site+'cadastroUsuario';
    }

    if(senha == confirmaSenha){
        $.ajax({
            url: url_site+'api/cadastraUsuario.php',
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function(data){
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main', urlRedir);
                    $('.buttonEnviar').attr('disabled', 'disabled');
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
    }else{
        myAlert('danger','Confimação de senha incorreta.', 'main');
    }
    return false;
})


$('#listaUsuario').on('click','.removerUsuario' ,function(){
    var idRegistro = $(this).attr('data-id');
    $.ajax({
        url: url_site+'api/deletaUsuario.php',
        dataType: 'json',
        type: 'POST',
        data: { id: idRegistro},
        success : function(data){
            if(data.status == 'success'){
                myAlert(data.status,data.msg,'main', url_site+'listaUsuario');
            } else {
                myAlert(data.status,data.msg,'main');
            }
        }
    })
    return false;
});

// controlador do filtro
$('#filtro').submit(function(){
    var pagina = $('input[name="page"]').val();
    var termo1 = $('.termo1').val();
    var termo2 = $('.termo2').val();

    termo1 = (termo1) ? termo1+'/' : '';
    termo2 = (termo2) ? termo2+'/' : '';

    window.location.href = url_site+pagina+'/busca/'+termo1+termo2

    return false;
});



$('.termo1, .termo2').on('keyup focusout change',function(){
    var termo1 = $('.termo1').val();
    var termo2 = $('.termo2').val();
        if(termo1 || termo2){
            $('button[type="submit"]').prop('disabled', false);
        }else{
            $('button[type="submit"]').prop('disabled', true);
        }
    })
});


//mascaras
$('input[name="cpf"]').mask('000.000.000-00')
$('input[name="cnpj"]').mask('00.000.000/0000-00')
$('input[name="telefone"]').mask('(99) 99999-9999')
$('input[name="cep"]').mask('00000-000')


function myAlert(tipo, mensagem, pai, url){
    url = (url == undefined) ? url = '' : url = url;
    componente = '<div class="alert alert-'+tipo+'" role="alert">'+mensagem+'</div>';

    $(pai).prepend(componente);

    setTimeout(function(){
        $(pai).find('div.alert').remove();
        //vai redirecionar?
        if(tipo == 'success' && url){
            setTimeout(function(){
                window.location.href = url;
            },300);
        }
    },1500)

}