-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.22-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para bd_sistema
CREATE DATABASE IF NOT EXISTS `bd_sistema` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `bd_sistema`;

-- Copiando estrutura para tabela bd_sistema.t_administradora
CREATE TABLE IF NOT EXISTS `t_administradora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '0',
  `cnpj` varchar(14) NOT NULL DEFAULT '0',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_administradora: ~6 rows (aproximadamente)
DELETE FROM `t_administradora`;
/*!40000 ALTER TABLE `t_administradora` DISABLE KEYS */;
INSERT INTO `t_administradora` (`id`, `nome`, `cnpj`, `dataCadastro`) VALUES
	(1, 'Trend Condomínio', '12345678912345', '2022-04-11 08:36:44'),
	(2, 'Administração Três Lares', '12345678912345', '2022-04-11 08:36:47'),
	(3, 'Adm Miranda', '12345678912345', '2022-04-11 08:36:48'),
	(10, 'Letícia ADMs', '12345678912345', '2022-04-11 08:36:48'),
	(12, 'Athena Condo', '12345678912345', '2022-04-11 08:36:49'),
	(13, 'Fenix Administração de Condomínios', '12345678912345', '2022-04-11 08:36:49');
/*!40000 ALTER TABLE `t_administradora` ENABLE KEYS */;

-- Copiando estrutura para tabela bd_sistema.t_bloco
CREATE TABLE IF NOT EXISTS `t_bloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL DEFAULT 0,
  `nomeBloco` varchar(50) NOT NULL DEFAULT '0',
  `andares` int(11) NOT NULL DEFAULT 0,
  `qtdUnidades` int(11) NOT NULL DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chBCondominio` (`from_condominio`),
  CONSTRAINT `chBCondominio` FOREIGN KEY (`from_condominio`) REFERENCES `t_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_bloco: ~13 rows (aproximadamente)
DELETE FROM `t_bloco`;
/*!40000 ALTER TABLE `t_bloco` DISABLE KEYS */;
INSERT INTO `t_bloco` (`id`, `from_condominio`, `nomeBloco`, `andares`, `qtdUnidades`, `dataCadastro`) VALUES
	(1, 1, 'Bloco A', 3, 6, '2022-04-07 15:35:48'),
	(2, 1, 'Bloco B', 3, 2, '2022-03-29 14:22:06'),
	(3, 2, 'Bloco Único', 2, 3, '2022-03-29 14:22:07'),
	(4, 3, 'Bloco 01', 2, 3, '2022-03-29 14:22:08'),
	(5, 3, 'Bloco 02', 3, 2, '2022-03-29 14:22:10'),
	(6, 5, 'Bloco 01', 5, 2, '2022-03-30 13:37:58'),
	(7, 6, 'Bloco A', 3, 2, '2022-03-30 13:38:14'),
	(8, 11, 'Bloco A', 8, 2, '2022-03-30 13:38:29'),
	(9, 9, 'Bloco A', 2, 2, '2022-03-30 16:46:11'),
	(10, 10, 'Bloco B', 3, 2, '2022-03-30 16:46:34'),
	(14, 12, 'Bloco único', 15, 2, '2022-04-01 16:25:53'),
	(15, 8, 'Bloco 01', 2, 2, '2022-04-04 11:44:42'),
	(16, 4, 'Bloco A', 3, 2, '2022-04-06 12:54:59'),
	(20, 27, 'Bloco A', 3, 2, '2022-04-11 11:30:49');
/*!40000 ALTER TABLE `t_bloco` ENABLE KEYS */;

-- Copiando estrutura para tabela bd_sistema.t_condominio
CREATE TABLE IF NOT EXISTS `t_condominio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_administradora` int(11) NOT NULL,
  `nomeCondominio` varchar(255) NOT NULL DEFAULT '0',
  `qtdBlocos` int(11) NOT NULL DEFAULT 0,
  `lougradouro` varchar(255) NOT NULL DEFAULT '0',
  `numero` int(8) NOT NULL DEFAULT 0,
  `bairro` varchar(255) NOT NULL DEFAULT '0',
  `cidade` varchar(255) NOT NULL DEFAULT '0',
  `estado` varchar(255) NOT NULL DEFAULT '0',
  `cep` varchar(8) NOT NULL DEFAULT '0',
  `sindico` varchar(255) NOT NULL DEFAULT '0',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chAdm` (`from_administradora`) USING BTREE,
  CONSTRAINT `chAdm` FOREIGN KEY (`from_administradora`) REFERENCES `t_administradora` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_condominio: ~16 rows (aproximadamente)
DELETE FROM `t_condominio`;
/*!40000 ALTER TABLE `t_condominio` DISABLE KEYS */;
INSERT INTO `t_condominio` (`id`, `from_administradora`, `nomeCondominio`, `qtdBlocos`, `lougradouro`, `numero`, `bairro`, `cidade`, `estado`, `cep`, `sindico`, `dataCadastro`) VALUES
	(1, 1, 'Villa Azul', 2, 'Rua Feliz', 125, 'Centro', 'Alagoas', 'SC', '89666666', 'Letícia Borchardt', '2022-04-07 15:35:04'),
	(2, 1, 'Condomínio Rosa', 1, 'Rua Fofinha', 504, 'Das Flores', 'City', 'PR', '10121123', 'Alanis', '2022-03-30 13:09:28'),
	(3, 1, 'Residencial Hortências', 2, 'Rua Beco', 1111, 'Afastado', 'Timbó', 'SC', '89120000', 'Luan', '2022-03-30 13:09:29'),
	(4, 2, 'Park Avenue Place', 3, 'Av. Caribe', 112, 'Centro', 'São Paulo', 'SP', '45645678', 'Paulo', '2022-03-30 13:13:37'),
	(5, 2, 'Condomínio Museum Tower', 2, 'Rua Freitas', 4895, 'Amazonas', 'Blumenau', 'SC', '00000000', 'João', '2022-03-30 13:14:40'),
	(6, 2, 'Residencial Centria', 4, 'Rua Amazonas', 1000, 'Dos Estados', 'Timbó ', 'SC', '89120000', 'Pedro', '2022-03-30 13:16:14'),
	(7, 2, 'The Atelier Residence', 2, 'Rua Canarinhos', 568, 'Floresta', 'Pernambuco', 'RJ', '12345678', 'Fernanda', '2022-03-30 13:17:06'),
	(8, 3, 'Residencial Quatro Estações', 3, 'Av. Jonas Pessoas', 1258, 'Centro', 'Indaial', 'SC', '45687978', 'Joana', '2022-03-30 13:18:04'),
	(9, 3, 'Forward Building', 2, 'Rua Augusto Húngaro', 41, 'Bom Viver', 'Lins', 'AM', '15975323', 'Felipe', '2022-03-30 13:19:34'),
	(10, 3, 'Residencial Jardim Canadá ', 1, 'Rua Antônio Eduardo', 123, 'Itália', 'Carajé', 'SP', '12345689', 'Amanda', '2022-03-30 13:21:30'),
	(11, 3, 'Prima Chelsea', 2, 'Av. Dom Pedro II', 457, 'Vila Nova', 'Benedito Novo', 'SC', '00100203', 'Antônia', '2022-03-30 13:22:35'),
	(12, 1, 'The Orion Building', 1, 'Av. Carlos Gomes', 1114, 'Roma', 'Blumenau', 'SP', '45678900', 'Carlos', '2022-03-30 13:23:44'),
	(20, 3, 'Condomínio Belo', 5, 'Rua Belém', 5, 'Arapongas', 'Blumenau', 'RJ', '89120000', 'Ana ', '2022-03-31 13:06:00'),
	(22, 2, 'Vale das Flores', 2, 'Rua ruazinha', 2222, 'Centro', 'Timbó', 'PB', '89120000', 'Letícia', '2022-04-06 13:07:58'),
	(23, 13, 'Condomínio Fofinho', 5, 'Rua Belém', 1, 'Centro', 'Blumenau', 'PI', '89120000', 'Ana ', '2022-04-11 08:56:15'),
	(27, 3, 'Condomínio LB', 2, 'Rua Acaro', 125, 'Centro', 'Rio de Janeiro', 'SC', '1234568', 'Ana Paula Santos', '2022-04-11 11:22:51');
/*!40000 ALTER TABLE `t_condominio` ENABLE KEYS */;

-- Copiando estrutura para tabela bd_sistema.t_conselho
CREATE TABLE IF NOT EXISTS `t_conselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '0',
  `from_funcao` enum('Subsíndico','Conselheiro') NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chCCondominio` (`from_condominio`),
  CONSTRAINT `chCCondominio` FOREIGN KEY (`from_condominio`) REFERENCES `t_condominio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_conselho: ~6 rows (aproximadamente)
DELETE FROM `t_conselho`;
/*!40000 ALTER TABLE `t_conselho` DISABLE KEYS */;
INSERT INTO `t_conselho` (`id`, `from_condominio`, `nome`, `from_funcao`, `dataCadastro`) VALUES
	(2, 9, 'Pablo', 'Subsíndico', '2022-03-30 16:32:18'),
	(3, 9, 'Diogo', 'Conselheiro', '2022-03-30 14:14:47'),
	(4, 10, 'Leandro', 'Subsíndico', '2022-03-30 14:15:04'),
	(7, 1, 'Rodrigo', 'Subsíndico', '2022-03-30 16:32:23'),
	(12, 27, 'Letícia Borchardt', 'Conselheiro', '2022-04-11 12:43:16');
/*!40000 ALTER TABLE `t_conselho` ENABLE KEYS */;

-- Copiando estrutura para tabela bd_sistema.t_convidados
CREATE TABLE IF NOT EXISTS `t_convidados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `convidado` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(20) NOT NULL DEFAULT '',
  `telefone` varchar(20) NOT NULL DEFAULT '',
  `from_reserva_salao_festas` int(11) NOT NULL,
  `from_unidade` int(11) NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `dataUpdate` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chReserva` (`from_reserva_salao_festas`),
  KEY `FK_t_convidados_t_unidade` (`from_unidade`),
  CONSTRAINT `FK_t_convidados_t_unidade` FOREIGN KEY (`from_unidade`) REFERENCES `t_unidade` (`id`),
  CONSTRAINT `chReserva` FOREIGN KEY (`from_reserva_salao_festas`) REFERENCES `t_reserva_salao_festas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_convidados: ~24 rows (aproximadamente)
DELETE FROM `t_convidados`;
/*!40000 ALTER TABLE `t_convidados` DISABLE KEYS */;
INSERT INTO `t_convidados` (`id`, `convidado`, `cpf`, `telefone`, `from_reserva_salao_festas`, `from_unidade`, `dataCadastro`, `dataUpdate`) VALUES
	(1, 'Pedro', '123.456.789-12', '47 9999-9999', 3, 3, '2022-03-30 14:57:50', '2022-03-31 14:05:02'),
	(2, 'José', '456.897.897-89', '47 9999-9999', 3, 3, '2022-03-30 14:57:56', '2022-03-31 14:05:02'),
	(3, 'Paula', '111.111.111-12', '47 9999-9999', 3, 3, '2022-03-30 14:57:59', '2022-03-31 14:05:02'),
	(4, 'Júlia', '225.555.555-87', '47 9999-9999', 3, 3, '2022-03-30 14:58:00', '2022-03-31 14:05:02'),
	(5, 'Cibele', '555.555.555-89', '47 9999-9999', 3, 3, '2022-03-30 14:58:01', '2022-03-31 14:05:02'),
	(6, 'Mariane', '000.000.000-12', '47 9999-9999', 3, 3, '2022-03-30 14:58:02', '2022-03-31 14:05:02'),
	(7, 'Frederico', '888.999.977-00', '47 9999-9999', 3, 3, '2022-03-30 14:58:03', '2022-03-31 14:05:02'),
	(8, 'Cristoffer', '001.002.003-89', '47 9999-9999', 3, 3, '2022-03-30 14:58:04', '2022-03-31 14:05:02'),
	(9, 'Gisele', '001.002.003-89', '47 9999-9999', 2, 2, '2022-03-30 14:58:07', '2022-03-31 14:05:02'),
	(10, 'Larissa', '001.002.003-89', '47 9999-9999', 2, 2, '2022-03-30 14:58:08', '2022-03-31 14:05:02'),
	(11, 'Sayonara', '001.002.003-89', '47 9999-9999', 2, 2, '2022-03-30 14:58:10', '2022-03-31 14:05:02'),
	(12, 'Yasmin', '456.456.879-45', '47 9999-9999', 2, 2, '2022-03-30 14:58:11', '2022-03-31 14:05:02'),
	(13, 'Daniel', '001.002.003-89', '47 9999-9999', 2, 2, '2022-03-30 14:58:11', '2022-03-31 14:05:02'),
	(14, 'Matheus', '123.456.789-12', '47 9999-9999', 2, 2, '2022-03-30 14:58:12', '2022-03-31 14:05:02'),
	(15, 'Lucas', '001.002.003-89', '47 9999-9999', 2, 2, '2022-03-30 14:58:13', '2022-03-31 14:05:02'),
	(16, 'David', '225.555.555-87', '47 9999-9999', 2, 2, '2022-03-30 14:58:13', '2022-03-31 14:05:02'),
	(17, 'Juliana', '225.555.555-87', '47 9999-9999', 4, 7, '2022-03-30 14:59:12', '2022-03-31 14:05:02'),
	(18, 'Patricia', '225.555.555-87', '47 9999-9999', 4, 7, '2022-03-30 14:59:17', '2022-03-31 14:05:02'),
	(19, 'William', '225.555.555-87', '47 9999-9999', 4, 7, '2022-03-30 14:59:23', '2022-03-31 14:05:02'),
	(20, 'Rosa', '225.555.555-87', '47 9999-9999', 4, 7, '2022-03-30 14:59:29', '2022-03-31 14:05:02'),
	(21, 'Rosane', '225.555.555-87', '47 9999-9999', 4, 7, '2022-03-30 14:59:32', '2022-03-31 14:05:02'),
	(22, 'Liandra', '225.555.555-87', '47 9999-9999', 4, 7, '2022-03-30 14:59:42', '2022-03-31 14:05:02'),
	(23, 'Nicolas', '225.555.555-87', '47 9999-9999', 4, 7, '2022-03-30 14:59:48', '2022-03-31 14:05:02'),
	(24, 'Iago', '225.555.555-87', '47 9999-9999', 4, 7, '2022-03-30 15:00:14', '2022-03-31 14:05:02');
/*!40000 ALTER TABLE `t_convidados` ENABLE KEYS */;

-- Copiando estrutura para tabela bd_sistema.t_morador
CREATE TABLE IF NOT EXISTS `t_morador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL DEFAULT 0,
  `from_bloco` int(11) NOT NULL DEFAULT 0,
  `from_unidade` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `telefone` varchar(50) DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chMBloco` (`from_bloco`),
  KEY `chMCondominio` (`from_condominio`),
  KEY `chMUnidade` (`from_unidade`),
  CONSTRAINT `chMBloco` FOREIGN KEY (`from_bloco`) REFERENCES `t_bloco` (`id`),
  CONSTRAINT `chMCondominio` FOREIGN KEY (`from_condominio`) REFERENCES `t_condominio` (`id`),
  CONSTRAINT `chMUnidade` FOREIGN KEY (`from_unidade`) REFERENCES `t_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_morador: ~22 rows (aproximadamente)
DELETE FROM `t_morador`;
/*!40000 ALTER TABLE `t_morador` DISABLE KEYS */;
INSERT INTO `t_morador` (`id`, `from_condominio`, `from_bloco`, `from_unidade`, `nome`, `cpf`, `email`, `telefone`, `dataCadastro`) VALUES
	(4, 3, 4, 4, 'Letícia Borchardt', '12345678912', 'leti@emailg.com', '78544545454', '2022-04-11 09:04:25'),
	(5, 1, 2, 5, 'Alanis Santos', '12345678912', 'alanis@gmail.com', '1234567891', '2022-04-11 09:04:34'),
	(6, 1, 2, 5, 'Josefino Prado', '12345678912', 'ze@email.com', '', '2022-04-11 09:06:00'),
	(7, 5, 6, 7, 'Alan Santo', '12345678912', 'alan@email.com', '', '2022-04-11 09:06:01'),
	(9, 6, 7, 7, 'Godofredo Paulo', '12345678912', 'god@gmail.com', '', '2022-04-11 09:06:02'),
	(10, 11, 8, 8, 'Raquel Freitas', '12345678912', 'ra@gmail.com', '', '2022-04-11 09:06:02'),
	(11, 10, 10, 10, 'Fernando Freitas', '12345678912', 'fer@gmail.com', '4799998585', '2022-04-11 09:04:37'),
	(12, 9, 9, 9, 'Arthur Mendes', '12345678912', 'arthur@gmail.com', '4799990000', '2022-04-11 09:04:41'),
	(13, 10, 10, 11, 'Lara Longo', '12345678912', 'lara@gmail.com', '', '2022-04-11 09:06:04'),
	(14, 10, 10, 12, 'Joana Meideiros', '12345678912', 'jo@email.com', '1234789999', '2022-04-11 09:04:44'),
	(17, 12, 14, 17, 'Marvin Tabosa Fartaria', '12345678912', 'marvintabosa@email.com', '8232093436', '2022-04-11 09:04:50'),
	(18, 12, 14, 18, 'Janice Bacelar Raminhos', '12345678912', 'janiceacelar@gmail.com', '7336856101', '2022-04-11 09:04:55'),
	(19, 8, 15, 19, 'Mikaela Letras Martins', '12345678912', 'Mikaela@email.com', '7523694191', '2022-04-11 09:05:01'),
	(20, 8, 15, 20, 'Antonio Lampreia Basílio', '12345678912', 'antonioa@email.com', '0012345678', '2022-04-11 09:02:49'),
	(21, 8, 15, 21, 'Viviana Castanheira Câmara', '12345678912', 'Viviana@hotmail.com', '2735341837', '2022-04-11 09:05:06'),
	(22, 8, 15, 22, 'Jerónimo Modesto Melancia', '12345678912', 'Modesto@gmail.com', '6238154343', '2022-04-11 09:05:10'),
	(23, 1, 2, 5, 'Luana Mendes', '12345678912', 'luana@a.com', '', '2022-04-11 08:37:35'),
	(24, 4, 16, 23, 'João Saltão Gouveia', '12345678912', 'aa@a.com', '4899999999', '2022-04-11 09:05:13'),
	(25, 4, 16, 24, 'Hilário Laranjeira Colares', '12345678912', 'aa@a.com', '4499999999', '2022-04-11 09:05:20'),
	(26, 4, 16, 25, 'Isaac Aquino Ornelas', '12345678912', 'aa@a.com', '', '2022-04-11 08:37:37'),
	(27, 4, 16, 26, 'Sílvia Carreiro Carvalheiro', '12345678912', 'aa@a.com', '', '2022-04-11 08:37:37'),
	(31, 10, 10, 13, 'Letícia Borchardt', '12345678912', 'aa@a.com', '4899999999', '2022-04-11 09:05:22'),
	(34, 20, 15, 1, 'teste', '00000000000', 'aa@a.com', NULL, '2022-04-11 11:55:26');
/*!40000 ALTER TABLE `t_morador` ENABLE KEYS */;

-- Copiando estrutura para tabela bd_sistema.t_pet
CREATE TABLE IF NOT EXISTS `t_pet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomePet` varchar(100) NOT NULL DEFAULT '0',
  `tipo` enum('Cachorro','Gato','Pássaro') NOT NULL,
  `from_morador` int(11) NOT NULL DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chPMorador` (`from_morador`),
  CONSTRAINT `chPMorador` FOREIGN KEY (`from_morador`) REFERENCES `t_morador` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_pet: ~3 rows (aproximadamente)
DELETE FROM `t_pet`;
/*!40000 ALTER TABLE `t_pet` DISABLE KEYS */;
INSERT INTO `t_pet` (`id`, `nomePet`, `tipo`, `from_morador`, `dataCadastro`) VALUES
	(1, 'Laila', 'Cachorro', 4, '2022-03-30 11:30:32'),
	(2, 'Otávio', 'Gato', 6, '2022-03-30 11:30:48'),
	(3, 'Pingo', 'Pássaro', 6, '2022-03-30 11:31:14');
/*!40000 ALTER TABLE `t_pet` ENABLE KEYS */;

-- Copiando estrutura para tabela bd_sistema.t_reserva_salao_festas
CREATE TABLE IF NOT EXISTS `t_reserva_salao_festas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tituloEvento` varchar(255) NOT NULL DEFAULT '0',
  `from_unidade` int(11) NOT NULL DEFAULT 0,
  `dataEvento` datetime NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chRUnidade` (`from_unidade`),
  CONSTRAINT `chRUnidade` FOREIGN KEY (`from_unidade`) REFERENCES `t_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_reserva_salao_festas: ~2 rows (aproximadamente)
DELETE FROM `t_reserva_salao_festas`;
/*!40000 ALTER TABLE `t_reserva_salao_festas` DISABLE KEYS */;
INSERT INTO `t_reserva_salao_festas` (`id`, `tituloEvento`, `from_unidade`, `dataEvento`, `dataCadastro`) VALUES
	(2, 'Festa', 2, '2022-04-10 15:00:00', '2022-03-30 14:41:28'),
	(3, 'Casamento', 3, '2022-05-25 14:00:00', '2022-03-30 14:41:56'),
	(4, 'Aniversário', 7, '2022-04-12 16:00:00', '2022-03-30 14:42:27');
/*!40000 ALTER TABLE `t_reserva_salao_festas` ENABLE KEYS */;

-- Copiando estrutura para tabela bd_sistema.t_unidade
CREATE TABLE IF NOT EXISTS `t_unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL DEFAULT 0,
  `from_bloco` int(11) NOT NULL DEFAULT 0,
  `nomeUnidade` varchar(255) NOT NULL DEFAULT '0',
  `metragem` float NOT NULL DEFAULT 0,
  `qtdGaragem` int(11) DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chUBloco` (`from_bloco`),
  KEY `chUCondominio` (`from_condominio`),
  CONSTRAINT `chUBloco` FOREIGN KEY (`from_bloco`) REFERENCES `t_bloco` (`id`),
  CONSTRAINT `chUCondominio` FOREIGN KEY (`from_condominio`) REFERENCES `t_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_unidade: ~26 rows (aproximadamente)
DELETE FROM `t_unidade`;
/*!40000 ALTER TABLE `t_unidade` DISABLE KEYS */;
INSERT INTO `t_unidade` (`id`, `from_condominio`, `from_bloco`, `nomeUnidade`, `metragem`, `qtdGaragem`, `dataCadastro`) VALUES
	(1, 1, 1, 'Apto 101', 12.5, 0, '2022-03-29 14:20:56'),
	(2, 2, 3, 'Apto 202', 54.4, 2, '2022-03-29 14:22:37'),
	(3, 3, 5, 'Apto 402', 87, 2, '2022-04-07 15:33:49'),
	(4, 3, 4, 'Apto 403', 48, 1, '2022-03-29 14:25:37'),
	(5, 1, 2, 'Apto 301', 64.7, 2, '2022-03-29 14:27:07'),
	(6, 5, 6, 'Apto 505', 30, 1, '2022-03-30 13:40:01'),
	(7, 6, 7, 'Apto 901', 50, 2, '2022-03-30 13:39:59'),
	(8, 11, 8, 'Apto 204', 80, 3, '2022-03-30 13:40:33'),
	(9, 9, 9, 'Apto 201', 59.8, 1, '2022-03-30 16:47:53'),
	(10, 10, 10, 'Apto 408', 100.5, 2, '2022-03-30 16:48:12'),
	(11, 10, 10, 'Apto 409', 120.5, 2, '2022-03-31 08:53:48'),
	(12, 10, 10, 'Apto 501', 89.8, 1, '2022-03-31 08:55:11'),
	(13, 10, 10, 'Apto 502', 68, 1, '2022-03-31 08:56:38'),
	(17, 12, 14, 'Apto 101', 89, 1, '2022-04-04 11:06:31'),
	(18, 12, 14, 'Apto 102', 100, 2, '2022-04-04 11:06:43'),
	(19, 8, 15, 'Apto 101', 100, 1, '2022-04-04 11:44:59'),
	(20, 8, 15, 'Apto 102', 120, 1, '2022-04-04 11:45:10'),
	(21, 8, 15, 'Apto 103', 103, 0, '2022-04-04 11:45:22'),
	(22, 8, 15, 'Apto 104', 140, 2, '2022-04-04 11:45:35'),
	(23, 4, 16, 'Apto 101', 125, 1, '2022-04-06 12:55:15'),
	(24, 4, 16, 'Apto 102', 120, 2, '2022-04-06 12:55:27'),
	(25, 4, 16, 'Apto 103', 152, 2, '2022-04-06 12:55:45'),
	(26, 4, 16, 'Apto 201', 120, 1, '2022-04-06 12:55:56'),
	(27, 4, 16, 'Apto 202', 200, 2, '2022-04-06 12:56:10'),
	(29, 12, 14, 'Apto 201', 15, 0, '2022-04-06 14:49:09');
/*!40000 ALTER TABLE `t_unidade` ENABLE KEYS */;

-- Copiando estrutura para tabela bd_sistema.t_usuario
CREATE TABLE IF NOT EXISTS `t_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `usuario` varchar(255) NOT NULL DEFAULT '',
  `senha` varchar(255) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_usuario: ~3 rows (aproximadamente)
DELETE FROM `t_usuario`;
/*!40000 ALTER TABLE `t_usuario` DISABLE KEYS */;
INSERT INTO `t_usuario` (`id`, `nome`, `usuario`, `senha`, `dataCadastro`) VALUES
	(1, 'Letícia', 'leticia', '202cb962ac59075b964b07152d234b70', '2022-04-06 15:39:02'),
	(19, 'Ana Júlia', 'aninha', '202cb962ac59075b964b07152d234b70', '2022-04-11 12:43:43');
/*!40000 ALTER TABLE `t_usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
