<?
Class Usuario extends Dao {
    protected $id;

    function __construct(){
    }

    function getUsuario($id = null){

        $qry = 'SELECT * FROM t_usuario';
        if ($id) {
            $qry .= ' WHERE id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function userExiste($user){
        $qry = "SELECT usuario FROM t_usuario WHERE usuario = '".$user."'";
        return $this->listarData($qry,true);
    }

    function setUsuario($dados) {
        $values = '';
        $sql = 'INSERT INTO t_usuario (';

        foreach($dados as $ch=>$value){
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').')';

        return $this->insertData($sql);
    }

    function editUsuario($dados){
        $values = '';
        $sql = 'UPDATE t_usuario SET';
 
        foreach ($dados as $ch => $value) {
            if ($ch != 'editar'){
             $sql .="`".$ch."` = '".$value."', ";
        }
    }
         $sql = rtrim($sql,', ');
         $sql .=' WHERE id='.$dados['editar'];
 
         return $this->updateData($sql);
    }
 
 
     function deletaUsuario($id) {
         return $this->deletar("DELETE FROM t_usuario WHERE id =".$id);
     }
 }

?>