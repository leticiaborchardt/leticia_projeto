<? 
Class Morador extends Unidade{
    protected $id;

    function __construct(){

    }

    function getMorador($id = null){

        $qry = 'SELECT 
        mor.id,
        cond.nomeCondominio,
        bloco.nomeBloco,
        uni.nomeUnidade,
        mor.nome,
        mor.cpf,
        mor.email,
        mor.telefone,
        mor.dataCadastro,
        mor.from_condominio,
        mor.from_bloco,
        mor.from_unidade
        
        FROM t_morador mor
        
        INNER JOIN t_condominio cond ON mor.from_condominio = cond.id 
        INNER JOIN t_bloco bloco ON mor.from_bloco = bloco.id 
        INNER JOIN t_unidade uni ON mor.from_unidade = uni.id';
        
         
        $contaTermos =count($this->busca);
        if($contaTermos > 0){
            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i ==0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'mor.'.$field.' = '.$termo.' AND ';
                        }
                        break;
                    default:
                        if(!empty($termo)){
                            $qry = $qry.'mor.'.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        if ($id) {
            $qry .= ' WHERE mor.id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

  
    function setMorador($dados) {
        $values = '';
        $sql = 'INSERT INTO t_morador (';
  
        foreach($dados as $ch=>$value){
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
  
        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').')';
  
        return $this->insertData($sql);
    }
  
    function editMorador($dados){
        $values = '';
        $sql = 'UPDATE t_morador SET';
  
        foreach ($dados as $ch => $value) {
            if ($ch != 'editar'){
            $sql .="`".$ch."` = '".$value."', ";
        }
    }
        $sql = rtrim($sql,', ');
        $sql .=' WHERE id='.$dados['editar'];
  
        return $this->updateData($sql);
    }
  
  
    function deletaMorador($id) {
        return $this->deletar("DELETE FROM t_morador WHERE id =".$id);
    }


    function getMoradorPorCondominio($id = null){
        $qry = 'SELECT
        cond.nomeCondominio,
        COUNT(mor.id) AS totalMoradores
        
        FROM t_morador mor
        LEFT JOIN t_condominio cond ON cond.id = mor.from_condominio  
        GROUP BY from_condominio';
        
            
        if ($id) {
            $qry .= ' WHERE cond.id ='.$id;
            $unique = true;
        }
        
        return $this->listarData($qry, $unique);
    }
}
?>