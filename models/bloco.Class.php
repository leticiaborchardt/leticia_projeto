<? 
Class Bloco extends Condominio {
    protected $id;

    function __construct(){
        return $_SESSION['bloco'];
    }

    function getBloco($id = null){

        $qry = 'SELECT 
        bloco.id,
        cond.nomeCondominio,
        bloco.nomeBloco,
        bloco.andares,
        bloco.qtdUnidades,
        bloco.dataCadastro,
        bloco.from_condominio
        
        FROM t_bloco bloco
        
        INNER JOIN t_condominio cond ON bloco.from_condominio = cond.id ';
        
        $contaTermos =count($this->busca);
        if($contaTermos > 0){
            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i ==0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'bloco.'.$field.' = '.$termo.' AND ';
                        }
                        break;
                    default:
                        if(!empty($termo)){
                            $qry = $qry.'bloco.'.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        
        if ($id) {
            $qry .= ' WHERE bloco.id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function getBlocoFromCond($cond){
        $qry = 'SELECT id, nomeBloco FROM t_bloco WHERE from_condominio = '.$cond;
        return $this->listarData($qry);
    }


    function setBloco($dados) {
        $values = '';
        $sql = 'INSERT INTO t_bloco (';

        foreach($dados as $ch=>$value){
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').')';

        return $this->insertData($sql);
    }

    function editBloco($dados){
        $values = '';
        $sql = 'UPDATE t_bloco SET';
 
        foreach ($dados as $ch => $value) {
            if ($ch != 'editar'){
             $sql .="`".$ch."` = '".$value."', ";
         }
     }
         $sql = rtrim($sql,', ');
         $sql .=' WHERE id='.$dados['editar'];
 
         return $this->updateData($sql);
     }
 
 
     function deletaBloco($id) {
         return $this->deletar("DELETE FROM t_bloco WHERE id =".$id);
     }
 }
?>