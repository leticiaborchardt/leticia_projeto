<? 
Class Unidade extends Bloco{
    protected $id;

    function __construct(){
        return $_SESSION['unidade'];
    }

    function getUnidade($id = null){

      $qry = 'SELECT 
      uni.id,
      cond.nomeCondominio,
      bloco.nomeBloco,
      uni.nomeUnidade,
      uni.metragem,
      uni.qtdGaragem,
      uni.dataCadastro,
      uni.from_condominio,
      uni.from_bloco
      
      FROM t_unidade uni
      
      INNER JOIN t_condominio cond ON uni.from_condominio = cond.id 
      INNER JOIN t_bloco bloco ON uni.from_bloco = bloco.id ';
      
      $contaTermos =count($this->busca);
        if($contaTermos > 0){
            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i ==0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'uni.'.$field.' = '.$termo.' AND ';
                        }
                        break;
                    default:
                        if(!empty($termo)){
                            $qry = $qry.'uni.'.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }

      if ($id) {
          $qry .= ' WHERE uni.id ='.$id;
          $unique = true;
        }
        return $this->listarData($qry, $unique);
  }

    function getUnidadesFromBloco($id){
        $qry = 'SELECT id, nomeUnidade FROM t_unidade WHERE from_bloco ='.$id;
        return $this->listarData($qry);
    }

    function setUnidade($dados) {
      $values = '';
      $sql = 'INSERT INTO t_unidade (';

      foreach($dados as $ch=>$value){
          $sql .='`'.$ch.'`, ';
          $values .= "'".$value."', ";
      }

      $sql = rtrim($sql,', ');
      $sql .=') VALUES ('.rtrim($values,', ').')';

      return $this->insertData($sql);
  }

  function editUnidade($dados){
      $values = '';
      $sql = 'UPDATE t_unidade SET';

      foreach ($dados as $ch => $value) {
          if ($ch != 'editar'){
           $sql .="`".$ch."` = '".$value."', ";
       }
   }
       $sql = rtrim($sql,', ');
       $sql .=' WHERE id='.$dados['editar'];

       return $this->updateData($sql);
   }


   function deletaUnidade($id) {
       return $this->deletar("DELETE FROM t_unidade WHERE id =".$id);
   }


}
