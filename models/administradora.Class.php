<? 
Class Administradora extends Dao {
    protected $id;

    function __construct(){
        return $_SESSION['adm'];
    }

    function getAdministradora($id = null){

        $qry = 'SELECT * FROM t_administradora';
        
        $contaTermos =count($this->busca);
        if($contaTermos > 0){
            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i ==0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.$field.' = '.$termo.' AND ';
                        }
                        break;
                    default:
                        if(!empty($termo)){
                            $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        
        if ($id) {
            $qry .= ' WHERE id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function getUltimasAdm($id = null){
        $qry = 'SELECT 
        nome
        FROM t_administradora 
        ORDER BY dataCadastro DESC
        
        LIMIT 5';
        if ($id) {
            $qry .= ' WHERE nome ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function setAdministradora($dados) {
        $values = '';
        $sql = 'INSERT INTO t_administradora (';

        foreach($dados as $ch=>$value){
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').')';
        
        return $this->insertData($sql);
    }


    function editAdministradora($dados){
       $values = '';
       $sql = 'UPDATE t_administradora SET';

       foreach ($dados as $ch => $value) {
           if ($ch != 'editar'){
            $sql .="`".$ch."` = '".$value."', ";
        }
    }
        $sql = rtrim($sql,', ');
        $sql .=' WHERE id='.$dados['editar'];

        return $this->updateData($sql);
    }


    function deletaAdministradora($id) {
        return $this->deletar("DELETE FROM t_administradora WHERE id =".$id);
    }
}
?>