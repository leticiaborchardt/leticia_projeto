<? 
Class Conselho extends Condominio {
    protected $id;

    function __construct(){
        return $_SESSION['conselho'];
    }

    function getConselho($id = null){

        $qry = 'SELECT 
        con.id,
        con.nome,
        con.from_funcao,
        cond.nomeCondominio,
        con.dataCadastro,
        con.from_condominio
        
        FROM t_conselho con
        
        INNER JOIN t_condominio cond ON con.from_condominio = cond.id';
        if ($id) {
            $qry .= ' WHERE con.id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function setConselho($dados) {
        $values = '';
        $sql = 'INSERT INTO t_conselho (';

        foreach($dados as $ch=>$value){
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').')';

        return $this->insertData($sql);
    }

    function editConselho($dados){
        $values = '';
        $sql = 'UPDATE t_conselho SET';
 
        foreach ($dados as $ch => $value) {
            if ($ch != 'editar'){
             $sql .="`".$ch."` = '".$value."', ";
         }
     }
         $sql = rtrim($sql,', ');
         $sql .=' WHERE id='.$dados['editar'];
 
         return $this->updateData($sql);
     }
 
 
     function deletaConselho($id) {
         return $this->deletar("DELETE FROM t_conselho WHERE id =".$id);
     }
 }
?>