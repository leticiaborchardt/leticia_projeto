<? 
Class Condominio extends Administradora {
    protected $id;

    function __construct(){
        return $_SESSION['condominio'];
    }

    function getCondominio($id = null){

        $qry = 'SELECT 
        cond.id,
        adm.nome,
        cond.nomeCondominio,
        cond.qtdBlocos,
        cond.lougradouro,
        cond.numero,
        cond.bairro,
        cond.cidade,
        cond.estado,
        cond.cep,
        cond.sindico,
        cond.dataCadastro,
        cond.from_administradora
        
        FROM t_condominio cond
        
        INNER JOIN t_administradora adm ON cond.from_administradora = adm.id ';
        
        $contaTermos =count($this->busca);
        if($contaTermos > 0){
            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i ==0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.'cond.'.$field.' = '.$termo.' AND ';
                        }
                        break;
                    default:
                        if(!empty($termo)){
                            $qry = $qry.'cond.'.$field.' LIKE "%'.$termo.'%" AND ';
                        }
                        break;
                }
            }
            $qry = rtrim($qry, ' AND');
        }
        
        if ($id) {
            $qry .= ' WHERE cond.id ='.$id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function setCondominio($dados) {
        $values = '';
        $sql = 'INSERT INTO t_condominio (';

        foreach($dados as $ch=>$value){
            $sql .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }

        $sql = rtrim($sql,', ');
        $sql .=') VALUES ('.rtrim($values,', ').')';

        return $this->insertData($sql);
    }

    function editCondominio($dados){
        $values = '';
        $sql = 'UPDATE t_condominio SET';
 
        foreach ($dados as $ch => $value) {
            if ($ch != 'editar'){
             $sql .="`".$ch."` = '".$value."', ";
         }
     }
         $sql = rtrim($sql,', ');
         $sql .=' WHERE id='.$dados['editar'];
 
         return $this->updateData($sql);
     }
 
 
     function deletaCondominio($id) {
         return $this->deletar("DELETE FROM t_condominio WHERE id =".$id);
     }
 }
?>