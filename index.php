<?
include "includes/uteis.php";

$user = new Restrito();
if(!$user->acesso()){
    header("Location: login.php");
}

if($_GET['page'] == 'logout'){
    if ($user->logout()) {
        header('Location: '.$url_site.'login.php');
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gestão de Condomínios</title>
    <link rel="stylesheet" href="<?=$url_site?>css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="<?=$url_site?>css/app.css">
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark container-fluid">
        <a class="navbar-brand mr-4" href="<?=$url_site?>"><i class="bi bi-briefcase-fill"></i> Home</a>
        <div class="dropdown mr-3">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="navCadastros" data-toggle="dropdown" aria-expanded="false">
                Cadastros
            </button>
            <div class="dropdown-menu bg-secondary" aria-labelledby="navCadastros">
                <? foreach ($navCadastro as $ch => $menu) { ?>
                    <a class="nav-link bg-secondary text-white dropdown-item" href="<?=$url_site.$ch?>"><?= $menu ?></a>
                <? } ?>
            </div>
        </div>
        <div class="dropdown mr-5">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="navListas" data-toggle="dropdown" aria-expanded="false">
                Listagens
            </button>
            <div class="dropdown-menu bg-secondary" aria-labelledby="navListas">
                <? foreach ($navListagem as $ch => $menu) { ?>
                    <a class="nav-link bg-secondary text-white dropdown-item" href="<?=$url_site.$ch?>"><?= $menu ?></a>
                <? } ?>
            </div>
        </div>
        <div class="col d-flex justify-content-end">
            <?
                $nome = explode(' ', $_SESSION['USUARIO']['nome'])
            ?>
            <p class="mt-2 mr-3 text-white"><i class="bi bi-person-circle"></i> Bem-Vindo(a), <?=$nome[0]?> :)</p>
            <a href="<?=$url_site?>logout" class="nav-link text-light"><i class="bi bi-box-arrow-right h4"></i></a>
        </div>
    </nav>


    <main class="container mb-5">
        <?
        switch ($_GET['page']) {
            case '':
            case 'inicio':
                require "controllers/inicio.php";
                require "views/inicio.php";
                break;
            default:
                require 'controllers/'.$_GET['page'] . '.php';
                require 'views/'.$_GET['page'] . '.php';
                break;
        }
        ?>
    </main>


    <footer class="fixed-bottom">
        <div class="w-100 py-2 px-2 bg-dark text-white tx-small"><small>&copy; Todos os direitos reservados.</small></div>
    </footer>

    <script>
        var url_site = '<?=$url_site?>';
    </script>
    <script src="<?=$url_site?>js/jquery-3.6.0.min.js"></script>
    <script src="<?=$url_site?>js/bootstrap.min.js"></script>
    <script src="<?=$url_site?>js/bootstrap.bundle.min.js"></script>
    <script src="<?=$url_site?>js/jquery.mask.min.js"></script>
    <script src="<?=$url_site?>js/app.js?v=<?= rand(0, 9999) ?>"></script>
</body>

</html>