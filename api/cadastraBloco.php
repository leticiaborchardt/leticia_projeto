<?
require "../includes/uteis.php";

$bloco = new Bloco();
if ($bloco->setBloco($_POST)) {
    $result = array(
        "status" => "success",
        "msg" => "Novo bloco cadastrado com sucesso!",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "O cadastro não pôde ser efetuado.",
    );

    echo json_encode($result);
}
 
?>