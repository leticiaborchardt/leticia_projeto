<?
require "../includes/uteis.php";

$conselho = new Conselho();
$result = $conselho->deletaConselho($_POST['id']);
if ($result) {
    
    $totalRegistros = $conselho->getConselho()['totalResults'];

    $result = array(
        "status" => "success",
        "totalRegistros" => $totalRegistros,
        "msg" => "Seu registro foi deletado com sucesso!",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "Ocorreu um erro ao excluir o registro.",
    );

    echo json_encode($result);
}

?>