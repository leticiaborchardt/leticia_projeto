<?

use LDAP\Result;

require "../includes/uteis.php";

$adm = new Administradora();
$result = $adm->deletaAdministradora($_POST['id']);
if ($result) {

    $totalRegistros = $adm->getAdministradora()['totalResults'];

    $result = array(
        "status" => "success",
        "totalRegistros" => $totalRegistros,
        "msg" => "Seu registro foi deletado com sucesso!",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "Ocorreu um erro ao excluir o registro.",
    );

    echo json_encode($result);
}

?>