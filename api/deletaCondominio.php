<?
require "../includes/uteis.php";

$condominio = new Condominio();
$result = $condominio->deletaCondominio($_POST['id']);
if ($result) {
    
    $totalRegistros = $condominio->getCondominio()['totalResults'];
    

    $result = array(
        "status" => "success",
        "totalRegistros" => $totalRegistros,
        "msg" => "Seu registro foi deletado com sucesso!",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "Ocorreu um erro ao excluir o registro.",
    );

    echo json_encode($result);
}

?>