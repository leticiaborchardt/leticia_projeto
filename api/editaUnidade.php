<?
require "../includes/uteis.php";

$unidade = new Unidade();
if ($unidade->editUnidade($_POST)) {
    $result = array(
        "status" => "success",
        "msg" => "Registro editado com sucesso!",
    );

    echo json_encode($result);
    
} else {
    $result = array(
        "status" => "danger",
        "msg" => "O registro não pôde ser editado.",
    );

    echo json_encode($result);
}
?>