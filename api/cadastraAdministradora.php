<?
require "../includes/uteis.php";

$adm = new Administradora();
if ($adm->setAdministradora($_POST)) {
    $result = array(
        "status" => "success",
        "msg" => "Nova administradora cadastrada com sucesso!",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "O cadastro não pôde ser efetuado.",
    );

    echo json_encode($result);
}
 
?>