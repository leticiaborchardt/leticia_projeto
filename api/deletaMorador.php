<?
require "../includes/uteis.php";

$morador = new Morador();
$result = $morador->deletaMorador($_POST['id']);
if ($result) {
    
    $totalRegistros = $morador->getMorador()['totalResults'];

    $result = array(
        "status" => 'success',
        "totalRegistros" => $totalRegistros,
        "msg" => "Seu registro foi deletado com sucesso!"
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => 'danger',
        "msg" => "Ocorreu um erro ao excluir o registro."
    );

    echo json_encode($result);
}
?>