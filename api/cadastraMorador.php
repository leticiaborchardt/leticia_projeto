<?
require "../includes/uteis.php";

$morador = new Morador();
if($morador->setMorador($_POST)){
    $result = array(
        "status" => 'success',
        "msg" => "Novo morador cadastrado com sucesso!"
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => 'danger',
        "msg" => "O cadastro não pôde ser efetuado."
    );

    echo json_encode($result);
}
?>