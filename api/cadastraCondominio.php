<?
require "../includes/uteis.php";

$condominio = new Condominio();
if ($condominio->setCondominio($_POST)) {
    $result = array(
        "status" => "success",
        "msg" => "Novo condomínio cadastrado com sucesso!",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "O cadastro não pôde ser efetuado.",
    );

    echo json_encode($result);
}
 
?>