<?
require "../includes/uteis.php";

$unidades = new Unidade();
$dados = $unidades->getUnidadesFromBloco($_REQUEST['id']);

if(!empty($dados)){
    $result = array(
        "status" => 'success',
        "resultSet" => $dados['resultSet']
    );

} else{
    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pôde ser inserido."
    );
}
echo json_encode($result);    
?>