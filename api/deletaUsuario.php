<?
require "../includes/uteis.php";

$user = new Usuario();
$result = $user->deletaUsuario($_POST['id']);
if ($result) {
    
    $totalRegistros = $user->getUsuario()['totalResults'];
    

    $result = array(
        "status" => "success",
        "totalRegistros" => $totalRegistros,
        "msg" => "Seu registro foi deletado com sucesso!",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "Ocorreu um erro ao excluir o registro.",
    );

    echo json_encode($result);
}

?>