<?
require "../includes/uteis.php";

$adm = new Administradora();
if ($adm->editAdministradora($_POST)) {
    $result = array(
        "status" => "success",
        "msg" => "Registro editado com sucesso!",
    );

    echo json_encode($result);
    
} else {
    $result = array(
        "status" => "danger",
        "msg" => "O registro não pôde ser editado.",
    );

    echo json_encode($result);
}
?>