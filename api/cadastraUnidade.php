<?
require "../includes/uteis.php";

$unidade = new Unidade();
if ($unidade->setUnidade($_POST)) {
    $result = array(
        "status" => "success",
        "msg" => "Nova unidade cadastrada com sucesso!",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "O cadastro não pôde ser efetuado.",
    );

    echo json_encode($result);
}
 
?>