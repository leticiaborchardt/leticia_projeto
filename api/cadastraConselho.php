<?
require "../includes/uteis.php";

$conselho = new Conselho();
if ($conselho->setConselho($_POST)) {
    $result = array(
        "status" => "success",
        "msg" => "Novo conselho cadastrado com sucesso!",
    );

    echo json_encode($result);
} else {
    $result = array(
        "status" => "danger",
        "msg" => "O cadastro não pôde ser efetuado.",
    );

    echo json_encode($result);
}
 
?>