<h3 class="text-center mt-5 mb-4">Administradoras Cadastradas</h3>
<div class="table-responsive">
    <table id="listaAdm" class="table table-striped my-4 table-hover shadow bg-white rounded">
    <tr><!------------- FILTRO  ------------->
            <td colspan="11">
                <form class="form-inline my-2 my-lg-0" id="filtro" method="GET">
                    <input type="hidden" name="page" value="listaAdministradora">
                    <input class="form-control mr-sm-2 shadow bg-white rounded termo1" type="search" placeholder="Buscar Administradora" aria-label="Search" name="b[nome]">
                    <button class="btn btn-outline-success my-2 my-sm-0 ml-2 shadow botao" type="submit" disabled><i class="bi bi-search"></i></button>
                    <a href='<?=$url_site?>listaAdministradora' class="btn btn-outline-info my-2 my-sm-0 ml-2 shadow">Limpar Busca</i></a>
                </form>
            </td>
        </tr><!---------- FIM DO FILTRO  ---------->
    <tr>
        <th scope="col">Nome</td>
        <th scope="col">CNPJ</td>
        <th scope="col">Data Cadastro</th>
        <th scope="col" colspan="2">Ações</th>
    </tr>
    <? foreach ($result['resultSet'] as $chDados => $dadosAdm) { ?> 
        <tr data-id="<?=$dadosAdm['id']?>">
            <td><?=$dadosAdm['nome'] ?></td>
            <td><?=$dadosAdm['cnpj'] ?></td>
            <td><?=dateFormat($dadosAdm['dataCadastro']) ?></td>
            <td>
                <a class="text-dark h4" href="<?=$url_site?>cadastroAdministradora/id/<?=$dadosAdm['id']?>"><i class="bi bi-pencil-square"></i></a>
                <a class="text-dark h4 removerAdministradora"  href="#" data-id="<?=$dadosAdm['id']?>"><i class="bi bi-trash3-fill"></i></a>
            <td>
        </tr>
    <? } ?>
    </table>
</div>
<div class="row mb-5">
    <div class="col-12 col-md-4">
        <a href="<?=$url_site?>cadastroAdministradora" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Administradora</a>
    </div>
    <div class="col-12 col-md-4">
        <?=$paginacao?>
    </div>
    <div class="col-12 col-md-4">
        <p class="text-right ">Total de registros 
            <span class="badge badge-dark totalRegistros ">
                <?=($result['totalResults'])?>
            </span>
        </p>
    </div>
</div>

