<h3 class="text-center mt-5" >Cadastrar Unidades</h3>
<form id="cadastroUnidade" action="#" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="condominioBloco">A qual condomínio esta unidade pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded fromCondominio" name="from_condominio" required>
                <option value="" disabled selected>Selecione o Condomínio</option>
                <? foreach($listaCondo['resultSet'] as $dado){ ?>
                     <option value="<?=$dado['id']?>" <?=($dado['id'] == $popular['from_condominio'] ? 'selected' : '')?>><?=$dado['nomeCondominio']?></option>
                <?}?>
            </select>
        </div>
        <div class="form-group col-md-4">
            <label for="condominioBloco">A qual bloco esta unidade pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded fromBloco" name="from_bloco" required>
                <?
                if ($_GET['id']){
                    $blocos = $unidades->getBlocoFromCond($popular['from_condominio']);
                    foreach($blocos['resultSet'] as $bloco){
                ?>
                <option value="<?=$bloco['id']?>"<?=($bloco['id'] == $popular['from_bloco'] ? 'selected' : '')?>><?=$bloco['nomeBloco']?></option>
                <?} }?>
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="nomeUnidade">Nomenclatura da Unidade</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nomeUnidade" id="nomeUnidade" value="<?=$popular['nomeUnidade']?>" placeholder="Ex.: Apto 101"required>
        </div>
        <div class="form-group col-md-4">
            <label for="metragem">Metragem (m²)</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="metragem" id="metragem" value="<?=$popular['metragem']?>" required>
        </div>
        <div class="form-group col-md-4">
            <label for="qtdGaragem">Quantidade de Garagens</label>
            <input type="number" min="0" class="form-control shadow mb-3 bg-white rounded" name="qtdGaragem" id="qtdGaragem" value="<?=$popular['qtdGaragem']?>" required>
        </div>
    </div>

    <? if($_GET['id']){ ?>
        <input type="hidden" name="editar" value="<?=$_GET['id']?>">
    <? } ?>
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-success mr-2 buttonEnviar">Cadastrar</button>
            <a class="btn btn-sm btn-secondary" href="<?=$url_site?>listaUnidade" role="button">Ir para a listagem</a>
        </div>
    </div>
</form> 