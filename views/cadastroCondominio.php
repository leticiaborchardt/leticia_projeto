<h3 class="text-center mt-5" >Cadastrar Condomínios</h3>
<form id="cadastroCondominio" action="#" method="post">
    <div class="form-row mt-4">
        <div class="form-group col-md-12">
            <label for="from_administradora">Administradora</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="from_administradora" required>
                <option value="" >Selecione a Administradora</option>
                <? foreach($listaAdm['resultSet'] as $dado){?>
                        <option value="<?=$dado['id']?>" <?=($dado['id'] == $popular['from_administradora'] ? 'selected' : '')?>><?=$dado['nome']?></option>
                <?}?>
            </select>
        </div>
    </div>

    <div class="form-row mt-4">
        <div class="form-group col-md-9">
            <label for="nomeCondominio">Nome do condomínio</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nomeCondominio" id="nomeCondominio" value="<?=$popular['nomeCondominio']?>" required>
        </div>
        <div class="form-group col-md-3">
            <label for="qtdBlocos">Quantidade de Blocos</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="qtdBlocos" id="qtdBlocos" value="<?=$popular['qtdBlocos']?>" required>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <h5>Endereço</h5>
        </div>
        <div class="form-group col-md-7">
            <label for="lougradouro">Lougradouro</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="lougradouro" id="lougradouro" value="<?=$popular['lougradouro']?>">
        </div>
        <div class="form-group col-md-2">
            <label for="numero">Número</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="numero" value="<?=$popular['numero']?>" id="numero">
        </div>
        <div class="form-group col-md-3">
            <label for="bairro">Bairro</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="bairro" value="<?=$popular['bairro']?>" id="bairro">
        </div>
    </div>
    <div class="form-row">
         <div class="form-group col-md-6">
            <label for="cidade">Cidade</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="cidade" id="cidade" value="<?=$popular['cidade']?>">
        </div>
        <div class="form-group col-md-3">
            <label for="estado">Estado</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="estado" id="estado">
                <option value="" disabled selected>Selecione</option>
                <?
                foreach($estados as $id => $estado) {
                    echo '<option value="'.$id.'"'.($popular['estado'] == $id ? 'selected' : '' ).'>'.$estado.' </option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-3">
            <label for="cep">CEP</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="cep" value="<?=$popular['cep']?>" id="cep">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <h5>Síndico</h5>
        </div>
        <div class="form-group col-md-12">
            <label for="sindico">Nome completo do síndico(a)</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="sindico" id="sindico" value="<?=$popular['sindico']?>" required>
        </div>
    </div>

    <? if($_GET['id']){ ?> 
        <input type="hidden" name="editar" value="<?=$_GET['id']?>">
    <? } ?>
    
    <div class="row mb-5">
        <div class="col-12 mb-4">
            <button type="submit" class="btn btn-success mr-2 buttonEnviar">Cadastrar</button>
            <a class="btn btn-sm btn-secondary" href="<?=$url_site?>listaCondo" role="button">Ir para a listagem</a>
        </div>
    </div>
    
</form>

