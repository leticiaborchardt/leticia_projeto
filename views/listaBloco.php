<h3 class="text-center mt-5 mb-4">Blocos Cadastrados</h3>
<div class="table-responsive">
    <table id="listaBloco" class="table table-striped my-4 table-hover shadow bg-white rounded">
    <tr><!------------- FILTRO  ------------->
            <td colspan="11">
                <form class="form-inline my-2 my-lg-0" id="filtro" method="GET">
                    <input type="hidden" name="page" value="listaBloco">
                    <input class="form-control mr-sm-2 shadow bg-white rounded termo1" type="search" placeholder="Buscar Bloco" aria-label="Search" name="b[nomeBloco]">
                    <select name="b[from_condominio]" class="form-control shadow bg-white rounded termo2">
                        <option value="">Filtrar por Condomínio</option>
                        <? foreach ($listaCond['resultSet'] as $dado) { ?>
                            <option value="<?= $dado['id'] ?>"><?= $dado['nomeCondominio'] ?></option>
                        <? } ?>
                    </select>
                    <button class="btn btn-outline-success my-2 my-sm-0 ml-2 shadow botao" type="submit" disabled><i class="bi bi-search"></i></button>
                    <a href='<?=$url_site?>listaBloco' class="btn btn-outline-info my-2 my-sm-0 ml-2 shadow">Limpar Busca</i></a>
                </form>
            </td>
    </tr><!---------- FIM DO FILTRO  ---------->
    <tr>
        <th scope="col">Condomínio</td>
        <th scope="col">Nomenclatura</td>
        <th scope="col">Qtd. Andares</td>
        <th scope="col">Unidades por Andar</td>
        <th scope="col">Data Cadastro</th>
        <th scope="col" colspan="2">Ações</th>
    </tr>
    <? foreach ($result['resultSet'] as $chDados => $dadosBloco) {?>
        <tr data-id="<?=$dadosBloco['id']?>">
            <td><?=$dadosBloco['nomeCondominio'] ?></td>
            <td><?=$dadosBloco['nomeBloco'] ?></td>
            <td><?=$dadosBloco['andares'] ?></td>
            <td><?=$dadosBloco['qtdUnidades'] ?></td>
            <td><?=dateFormat($dadosBloco['dataCadastro']) ?></td>
            <td>
                <a class="text-dark h4" href="<?=$url_site?>cadastroBloco/id/<?=$dadosBloco['id']?>"><i class="bi bi-pencil-square"></i></a>
                <a class="text-dark h4 removerBloco"  href="#" data-id="<?=$dadosBloco['id']?>"><i class="bi bi-trash3-fill"></i></a>
            <td>
        </tr>
    <? } ?>
    </table>
</div>
<div class="row mb-5">
    <div class="col-12 col-md-4">
        <a href="<?=$url_site?>cadastroBloco" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Bloco</a>
    </div>
    <div class="col-12 col-md-4">
        <?=$paginacao?>
    </div>
    <div class="col-12 col-md-4">
        <p class="text-right ">Total de registros 
            <span class="badge badge-dark totalRegistros ">
                <?=($result['totalResults'])?>
            </span>
        </p>
    </div>
</div>

