<h3 class="text-center mt-5 mb-4">Conselhos Cadastrados</h3>
<div class="table-responsive">
    <table id="listaConselho" class="table table-striped my-4 table-hover shadow bg-white rounded">
    <tr>
        <th scope="col">Condomínio</td>
        <th scope="col">Nome Completo</td>
        <th scope="col">Função</td>
        <th scope="col">Data Cadastro</th>
        <th scope="col" colspan="2">Ações</th>
    </tr> 
    <? foreach ($result['resultSet'] as $chDados => $dadosConselho) { ?>
        <tr data-id="<?=$dadosConselho['id']?>">
            <td><?=$dadosConselho['nomeCondominio'] ?></td>
            <td><?=$dadosConselho['nome'] ?></td>
            <td><?=$dadosConselho['from_funcao'] ?></td>
            <td><?=dateFormat($dadosConselho['dataCadastro']) ?></td>
            <td>
                <a class="text-dark h4" href="<?=$url_site?>cadastroConselho/id/<?=$dadosConselho['id']?>"><i class="bi bi-pencil-square"></i></a>
                <a class="text-dark h4 removerConselho"  href="#" data-id="<?=$dadosConselho['id']?>"><i class="bi bi-trash3-fill"></i></a>
            <td>
        </tr>
    <? } ?>
    </table>
</div>
<div class="row mb-5">
    <div class="col-12 col-md-4">
        <a href="<?=$url_site?>cadastroConselho" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Conselho</a>
    </div>
    <div class="col-12 col-md-4">
        <?=$paginacao?>
    </div>
    <div class="col-12 col-md-4">
        <p class="text-right ">Total de registros 
            <span class="badge badge-dark totalRegistros ">
                <?=($result['totalResults'])?>
            </span>
        </p>
    </div>
</div>

