<div class="row mb-4">
    <div class="col-12 text-center">
        <img src="img/home.svg" alt="" style="width: 150px;" class="mt-4" >
    </div>
    <div class="col-12 text-center"><h1 class="text-center">Gestão de Condomínios</h1></div>
    <div class="col-12 text-center"><p>Simplicidade e agilidade para a sua organização de condomínios!</p></div>
</div>

<!-- row com os totais -->
<div class="row my-5">
    <div class="col-12 col-sm-6 col-md-3 mb-4">
        <div class="card mx-auto shadow rounded" style="background: #abe3e0;">
            <p class="text-center pt-3 h1"><?=$totalCond['totalResults']?></p>
            <div class="text-center pb-3">
                <h5 class="m-0">Condomínios</h5>
                <small>Cadastrados</small>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-3 mb-4 ">
        <div class="card mx-auto shadow bg-white rounded">
            <div class="totais">
                <p class="text-center pt-3 h1"><?=$totalBloco['totalResults']?></p>
            </div>
            <div class="text-center pb-3">
                <h5 class="m-0">Blocos</h5>
                <small>Cadastrados</small>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-3 mb-4 ">
        <div class="card mx-auto shadow bg-white rounded">
            <p class="text-center pt-3 h1"><?=($totalUni['totalResults'])?></p>
            <div class="text-center pb-3">
                <h5 class="m-0">Unidades</h5>
                <small>Cadastradas</small>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-3 mb-4">
        <div class="card mx-auto shadow rounded bg-white">
            <p class="text-center pt-3 h1"><?=$totalMor['totalResults']?></p>
            <div class="text-center pb-3">
                <h5 class="m-0">Moradores</h5>
                <small>Cadastrados</small>
            </div>
        </div>
    </div>
</div>

<!-- moradores por condominio -->
<div class="row mb-5">
    <div class="col-12 col-md-6">
        <div class="text-center">
            <img src="img/grafico.png" width=100% class="shadow bg-white rounded img-fluid ">
        </div>
    </div>
    <div class="col-12 col-md-6 ">
        <div class="shadow bg-white rounded border p-3">
            <h4 class="text-center p-3">Moradores por Condomínio</h4>
            <div style="max-height: 155px; overflow-y: scroll;">
                <? foreach ($morPorCond['resultSet'] as $chDados => $dados) { ?>
                <p class="mb-0"><?=$dados['nomeCondominio']?>
                    <span class="badge badge-warning totalRegistros ">
                        <?=($dados['totalMoradores'])?>
                    </span></p>
                    <hr>
                <? } ?>
            </div>
        </div>
    </div>
</div>

<!-- últimas adms cadastradas -->
<div class="row mb-5">
    <div class="col-12 col-md-6">
        <div class="shadow bg-white rounded border p-3">
            <h4 class="text-center mb-3">Últimas Administradoras Cadastradas</h4>
            <? foreach ($ultimasAdm['resultSet'] as $chDados => $dados) { ?>
                <p class="m-0 pb-2 pl-3"><i class="bi bi-check2-circle"></i> <?=$dados['nome']?></p>
            <? } ?>
        </div>
    </div>
    <div class="col-12 col-md-6 mb-5">
        <div class="text-center">
            <img src="img/grafico.png" class="shadow bg-white rounded img-fluid ">
        </div>
    </div>
</div>