<h3 class="text-center mt-5 mb-4">Condomínios Cadastrados</h3>
<div class="table-responsive">
    <table id="listaCondominio" class="table table-striped my-4 table-hover shadow bg-white rounded">
    <tr><!------------- FILTRO  ------------->
            <td colspan="11">
                <form class="form-inline my-2 my-lg-0" id="filtro" method="GET">
                    <input type="hidden" name="page" value="listaCondo">
                    <input class="form-control mr-sm-2 shadow bg-white rounded termo1" type="search" placeholder="Buscar Condomínio" aria-label="Search" name="b[nome]">
                    <select name="b[from_administradora]" class="form-control shadow bg-white rounded termo2">
                        <option value="">Filtrar por Administradora</option>
                        <? foreach ($listaAdm['resultSet'] as $dado) { ?>
                            <option value="<?= $dado['id'] ?>"><?= $dado['nome'] ?></option>
                        <? } ?>
                    </select>
                    <button class="btn btn-outline-success my-2 my-sm-0 ml-2 shadow botao" type="submit" disabled><i class="bi bi-search"></i></button>
                    <a href='<?=$url_site?>listaCondo' class="btn btn-outline-info my-2 my-sm-0 ml-2 shadow">Limpar Busca</i></a>
                </form>
            </td>
        </tr><!---------- FIM DO FILTRO  ---------->
    <tr>
        <th scope="col">Administradora</td>
        <th scope="col">Condomínio</td>
        <th scope="col">Blocos</td> 
        <th scope="col">Endereço</td>
        <th scope="col">Síndico</td>
        <th scope="col">Data Cadastro</th>
        <th scope="col" colspan="2">Ações</th>
    </tr>
    <? foreach ($result['resultSet'] as $chDados => $dadosCondo) {?>
         <tr data-id="<?=$dadosCondo['id']?>">
            <td><?=$dadosCondo['nome'] ?></td>
            <td><?=$dadosCondo['nomeCondominio'] ?></td>
            <td><?=$dadosCondo['qtdBlocos'] ?></td>
            <td><?=$dadosCondo['lougradouro'].', '.$dadosCondo['numero'].' | '.$dadosCondo['bairro'].' | '.$dadosCondo['cidade'].' - '.$dadosCondo['estado'].' | '.$dadosCondo['cep']?></td>
            <td><?=$dadosCondo['sindico'] ?></td>
            <td><?=dateFormat($dadosCondo['dataCadastro']) ?></td>
            <td>
                <a class="text-dark h4" href="<?=$url_site?>cadastroCondominio/id/<?=$dadosCondo['id']?>"><i class="bi bi-pencil-square"></i></a>
                <a class="text-dark h4 removerCondominio"  href="#" data-id="<?=$dadosCondo['id']?>"><i class="bi bi-trash3-fill"></i></a>
            <td>
        </tr>
    <? } ?>
    </table>
</div>
<div class="row mb-5">
    <div class="col-12 col-md-4">
        <a href="<?=$url_site?>cadastroCondominio" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Condomínio</a>
    </div>
    <div class="col-12 col-md-4">
        <?=$paginacao?>
    </div>
    <div class="col-12 col-md-4">
        <p class="text-right ">Total de registros 
            <span class="badge badge-dark totalRegistros ">
                <?=($result['totalResults'])?>
            </span>
        </p>
    </div>
</div>