<h3 class="text-center mt-5" >Cadastrar Administradoras</h3>
<form id="cadastroAdm" action="#" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="nome">Nome da Administradora</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" id="nome" value="<?=$popular['nome']?>" required>
        </div>
        <div class="form-group col-md-4">
            <label for="cnpj">CNPJ</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="cnpj" id="cnpj" value="<?=$popular['cnpj']?>" required>
        </div>
    </div>

    <? if($_GET['id']){ ?>
        <input type="hidden" name="editar" value="<?=$_GET['id']?>">
    <? } ?>
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-success mr-2 buttonEnviar">Cadastrar</button>
            <a class="btn btn-sm btn-secondary" href="<?=$url_site?>listaAdministradora" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>