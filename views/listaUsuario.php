<h3 class="text-center mt-5 mb-4">Usuários Cadastradas</h3>
<div class="table-responsive">
    <table id="listaUsuario" class="table table-striped my-4 table-hover shadow bg-white rounded">
    <tr>
        <th scope="col">Nome Completo</td>
        <th scope="col">Usuario de acesso</td>
        <th scope="col">Data Cadastro</th>
        <th scope="col" colspan="2">Ações</th>
    </tr>
    <? foreach ($result['resultSet'] as $chDados => $dado) { ?> 
        <tr data-id="<?=$dado['id']?>">
            <td><?=$dado['nome'] ?></td>
            <td><?=$dado['usuario'] ?></td>
            <td><?=dateFormat($dado['dataCadastro']) ?></td>
            <td>
                <a class="text-dark h4" href="<?=$url_site?>cadastroUsuario/id/<?=$dado['id']?>"><i class="bi bi-pencil-square"></i></a>
                <a class="text-dark h4 removerUsuario" href="#" data-id="<?=$dado['id']?>"><i class="bi bi-trash3-fill"></i></a>
            <td>
        </tr>
    <? } ?>
    </table>
</div>
<div class="row mb-5">
    <div class="col-12 col-md-4">
        <a href="<?=$url_site?>cadastroUsuario" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Usuário</a>
    </div>
    <div class="col-12 col-md-4">
        <?=$paginacao?>
    </div>
    <div class="col-12 col-md-4">
        <p class="text-right ">Total de registros 
            <span class="badge badge-dark totalRegistros ">
                <?=($result['totalResults'])?>
            </span>
        </p>
    </div>
</div>