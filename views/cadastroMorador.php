<h3 class="text-center mt-5" >Cadastrar Moradores</h3>
<form id="cadastroMorador" action="#" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-4">
        <label for="nomeCondominio">Condomínio</label>
            <select class="form-control shadow mb-3 bg-white rounded fromCondominio" name="from_condominio" id="nomeCondominio" required>
                <option value="" disabled selected>Selecione o Condomínio</option>
                <? foreach($listaCondo['resultSet'] as $dado){?>
                    <option value="<?=$dado['id']?>" <?=($dado['id'] == $popular['from_condominio'] ? 'selected' : '')?>><?=$dado['nomeCondominio']?></option>
               <?}?>
            </select>
        </div>
        <div class="form-group col-md-4">
        <label for="nomeBloco">Bloco</label>
            <select class="form-control shadow mb-3 bg-white rounded fromBloco" name="from_bloco" id="nomeBloco" required>
                <?
                if ($_GET['id']){
                    $blocos = $morador->getBlocoFromCond($popular['from_condominio']);
                    foreach($blocos['resultSet'] as $bloco){
                ?>
                <option value="<?=$bloco['id']?>"<?=($bloco['id'] == $popular['from_bloco'] ? 'selected' : '')?>><?=$bloco['nomeBloco']?></option>
                <?} }?>
            </select>
        </div>
        <div class="form-group col-md-4">
        <label for="nomeUnidade">Unidade</label>
            <select class="form-control shadow mb-3 bg-white rounded fromUnidade" name="from_unidade" id="nomeUnidade" required>
                <?
                if ($_GET['id']){
                    $unidades = $morador->getUnidadesFromBloco($popular['from_bloco']);
                    foreach($unidades['resultSet'] as $unidade){
                ?>
                <option value="<?=$unidade['id']?>"<?=($unidade['id'] == $popular['from_unidade'] ? 'selected' : '')?>><?=$unidade['nomeUnidade']?></option>
                <?} }?>
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="nome">Nome completo</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" id="nome" value="<?=$popular['nome']?>" required>
        </div>
        <div class="form-group col-md-6">
            <label for="cpf">CPF/CNPJ</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="cpf" id="cpf" value="<?=$popular['cpf']?>" required>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="email">E-mail</label>
            <input type="email" class="form-control shadow mb-3 bg-white rounded" name="email" id="email" value="<?=$popular['email']?>" required>
        </div>
        <div class="form-group col-md-4">
            <label for="tel">Telefone/Celular</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="telefone" value="<?=$popular['telefone']?>" id="tel">
        </div>
    </div>

    <? if($_GET['id']){ ?>
        <input type="hidden" name="editar" value="<?=$_GET['id']?>">
    <? } ?>
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-success mr-2 buttonEnviar">Cadastrar</button>
            <a class="btn btn-sm btn-secondary" href="<?=$url_site?>listaMorador" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>