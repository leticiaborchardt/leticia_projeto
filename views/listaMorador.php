<h3 class="text-center mt-5 mb-4">Moradores Cadastrados</h3>
<div class="table-responsive">
    <table id="listaMoradores" class="table table-striped my-3 table-hover shadow bg-white rounded">
        <tr><!------------- FILTRO  ------------->
            <td colspan="11">
                <form class="form-inline my-2 my-lg-0" id="filtro" method="GET">
                    <input type="hidden" name="page" value="listaMorador">
                    <input class="form-control mr-sm-2 shadow bg-white rounded termo1" type="search" placeholder="Buscar Morador" aria-label="Search" name="b[nome]">
                    <select name="b[from_condominio]" class="form-control shadow bg-white rounded termo2">
                        <option value="">Filtrar por Condomínio</option>
                        <? foreach ($listaCondo['resultSet'] as $dado) { ?>
                            <option value="<?= $dado['id'] ?>"><?= $dado['nomeCondominio'] ?></option>
                        <? } ?>
                    </select>
                    <button class="btn btn-outline-success my-2 my-sm-0 ml-2 shadow botao" type="submit" disabled><i class="bi bi-search"></i></button>
                    <a href='<?=$url_site?>listaMorador' class="btn btn-outline-info my-2 my-sm-0 ml-2 shadow">Limpar Busca</i></a>
                </form>
            </td>
        </tr><!---------- FIM DO FILTRO  ---------->
        <tr>
            <th scope="col">Condomínio</th>
            <th scope="col">Bloco</th>
            <th scope="col">Unidade</th>
            <th scope="col">Nome Completo</th>
            <th scope="col">CPF/CNPJ</th>
            <th scope="col">E-mail</th>
            <th scope="col">Telefone</th>
            <th scope="col">Data Cadastro</th>
            <th scope="col" colspan="2">Ações</th>
        </tr>
        <? foreach ($result['resultSet'] as $chDados => $dados) { ?>
            <tr data-id="<?= $dados['id'] ?>">
                <td><?= $dados['nomeCondominio'] ?></td>
                <td><?= $dados['nomeBloco'] ?></td>
                <td><?= $dados['nomeUnidade'] ?></td>
                <td><?= $dados['nome'] ?></td>
                <td><?= $dados['cpf'] ?></td>
                <td><?= $dados['email'] ?></td>
                <td><?= $dados['telefone'] ?></td>
                <td><?= dateFormat($dados['dataCadastro']) ?></td>
                <td>
                    <a class="text-dark h4" href="<?=$url_site?>cadastroMorador/id/<?= $dados['id']; ?>"><i class="bi bi-pencil-square"></i></a>
                    <a class="text-dark h4 removerMorador" href="#" data-id="<?= $dados['id'] ?>"><i class="bi bi-trash3-fill"></i></a>
                <td>
            </tr>
        <? } ?>
    </table>
</div>
<div class="row mb-5">
    <div class="col-12 col-md-4">
        <a href="<?=$url_site?>cadastroMorador" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Morador</a>
    </div>
    <div class="col-12 col-md-4">
        <?= $paginacao ?>
    </div>
    <div class="col-12 col-md-4">
        <p class="text-right ">Total de registros
            <span class="badge badge-dark totalRegistros ">
                <?= ($result['totalResults']) ?>
            </span>
        </p>
    </div>
</div>